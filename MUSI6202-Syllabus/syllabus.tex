\documentclass[letterpaper,oneside,10pt]{scrartcl}

\input{../include/config} 


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% DOCUMENT
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{document}

\title{MUSI 4459/6202: Syllabus}
\author{Music DSP}
\date{Spring 2023} %%If commented, the current date is used.
\maketitle

\pagestyle{plain} %Now display headings: headings / fancy / ...

\section*{Course Details}
    \begin{tabular}{ll}
        \textbf{class time} & MW 3:30--4:45pm \\
        \textbf{location} & WV 175\\
        \textbf{credits} & 3 credit hours
    \end{tabular}
\section*{Instructor Information}
    \begin{tabular}{lp{100mm}l}
        \textbf{} & \textbf{instructor} & \textbf{teaching assistant}\\
        \textbf{name} & Alexander Lerch & Karn Watcharasupat \\
        \textbf{email} & \url{alexander.lerch@gatech.edu} & \url{kwatchar3@gatech.edu} \\
        \textbf{location} & Couch 203 (840 McMillan St) & \\
        \textbf{office hours} & {by appointment: \href{https://go.oncehub.com/alexanderlerch}{https://go.oncehub.com/alexanderlerch}} & TBA/\href{https://outlook.office.com/bookwithme/user/2b5a48468fa04ac88cf39160e03ec9ee@gatech.edu?anonymous&ep=plink}{by appointment} \\
    \end{tabular}

\section{General Information}        
    \subsection{Course Description}
        Research in music, as well as music production and composition increasingly relies on sophisticated digital signal processing techniques. This course will review fundamental elements of digital audio signal processing, such as sinusoids, spectra, digital filters, and Fourier analysis and their application to music processing problems. We will discuss audio effects and techniques such as fast convolution, phase vocoder, reverb, chorus / flanger, pitch-shifting, time compression, etc. The class will include practical in-class exercises and assignments on frequently used music processing algorithms.
%practical lab sessions as well as presentations of state of the art papers and student's projects.

    \subsection{Prerequisites}
        Prior coursework in signals and systems is expected. Programming experience and familiarity with Matlab will be helpful.

    \subsection{Learning Outcomes}
        Upon successful completion of the course, the students will demonstrate 
        \begin{itemize}
            \item   the ability to comprehend typical representations of digital systems such as block diagrams and difference equations,
            \item   an understanding of typical transforms in DSP such as the Fourier transform or the Z-transform,
            \item   an understanding of typical signal processing approaches to audio and music signals,
            \item   the ability to use this understanding to design audio processing systems such as audio effects, and
            \item   the ability to implement such designs in a programming language such as Matlab.
        \end{itemize}

\section{Grading}
    The following evaluative tools will be utilized in measuring progress towards obtaining the learning outcomes:
    \smallskip
    
    \begin{tabular}{lll}
        \textbf{quizzes} & 15\%\\
        \textbf{assignments} & 40\%\\
        \textbf{midterm exam I} & 10\%\\
        \textbf{midterm exam II} & 10\%\\
        \textbf{denoising competition} & 20\%\\
        \textbf{class participation} & 5\% \\
    \end{tabular}
    
    \subsection{Description of Graded Components}
        \begin{itemize}
            \item   \textbf{quizzes: }\\
                Quizzes might take place unannounced at any time during the semester. %Each student will have one ``joker'', meaning that the worst result will be discarded and will not contribute to the overall grade. 
            \item   \textbf{assignments: }\\
                Assignments will be posted according to the tentative schedule outlined in Sect.~\ref{sec:outline}. All assignments will contribute to the assignment grade with equal weight.
            \item   \textbf{midterm exams: }\\
                The two exams will be during the semester (cf.\ tentative schedule in Sect.~\ref{sec:outline}). The final exam is replaced by the codec competition.
            \item   \textbf{competition: }\\
                Each group will submit Matlab code for the denoising competition. The goal is to improve the quality of a noisy audio signal with traditional signal processing methods in Matlab. The target metric is the average Signal-to-Noise Ratio of a hidden test set. Details on the competition procedure and grading method will be defined in an announcement within the first weeks of the semester.
            \item   \textbf{class participation:}\\
                Each student is expected to actively participate in class. This is measured through attendance, direct class participation with discussions and answers, and posts in the accompanying forums (MS Teams). Contributions to teams of multiple students are expected to be equal between teammates.
        \end{itemize}
        
    \subsection{Undergraduate vs.\ Graduate Student Tasks \& Grading}
        As compared to students enrolled in the graduate class, the following differences apply to undergraduate students:
        \begin{itemize}
            \item   a ``joker'' for the quizzes --- the worst quiz grade will be removed from grading,
            \item   assignments, quizzes, and exam points will be multiplied with a factor of $1.1$ (e.g., $50$ points will be counted as $55$) before capping at the maximum number of points.
        \end{itemize}
        
    \subsection{Grading and Grading Policies}
        \input{../include/grading} 
        

\section{Course Materials}
        \subsection{Recommended Reading}
            The class will be roughly based on 
            \begin{quote}
                \begin{itemize}
                    \item   Zolzer, Udo: ``Digital Audio Signal Processing'', Wiley 2008
                \end{itemize}
            \end{quote}
        
        %\subsection{}
            The following books can provide additional insights into the topics discussed in class
                \begin{quote}
                    \begin{itemize}
                        \item   \textit{DSP} --- Lyon, Richard: ``Understanding Digital Signal Processing'', Prentice Hall 2011
                        \item   \textit{Audio Effects} --- Zolzer, Udo: ``DAFX: Digital Audio Effects'', Wiley 2011
                    \end{itemize}
                \end{quote}

    %\subsection{Recommended Reading}
        %The following books are recommended:
    %\smallskip
    %
        %\begin{tabular}{p{.35\linewidth}p{.65\linewidth}}
            %\textbf{ Sutter, H. and Alexandrescu, A.} & ``C++ Coding Standards. 101 Rules, Guidelines, and Best Practices'' (2004). Pearson Education, ISBN: 0-32-111358-6\\
            %\textbf{  Meyers, S.} & ``Effective C++: 55 Specific Ways to Improve Your Programs and Designs'' (2005), Addison-Wesley Professional, 3rd Edition, ISBN: 978-0321334879
        %\end{tabular}
        
    \subsection{Software}
        The assignments, project work, and exercises will be done in Matlab. This might include implementation of functions for signal synthesis, system analysis (transfer functions), and signal processing (audio effects). Please note the following license information:
				\begin{quote}
						\url{www.matlab.gatech.edu}
				\end{quote}
        
        Additional tools and programming languages can be used if approved by the instructor.
        
\section{Course Expectations \& Guidelines}
    \subsection{Course Schedule}\label{sec:outline}
        \begin{tabular}{l|p{.5\textwidth}|p{.1\textwidth}|p{.1\textwidth}|l}
            \textbf{date} & \textbf{topics} & \textbf{exercise} & \textbf{assignment} & \textbf{notes}\\
            \hline\hline
            01/09 & introduction, signals, periodicity, random processes, pdf, expectation values/moments, correlation & correlation &  &  \\
            01/16 & convolution & FIR filter& filter \& convolution & \\
            01/23 & Fourier series \& Fourier transform& DFT & Fourier analysis & MLK Hldy.\\
            01/30 &Fourier transform, sampling, quantization, SNR, number formats& quantization&& \\
            02/06 &oversampling, dither, noise-shaping, non-linear quant.&&dither, ns  & \\
            02/13 &z-transform, digital audio filters, FIR/IIR, FFT filtering& biquad& & midterm I\\
            02/20 &sample rate conversion, real-time systems &resampling&& \\
            02/27 &denoising&&& \\
            03/06 &delay-based FX and reverb&vibrato& mod. fx&guthman?\\
            03/13 &dynamics processing& PPM & limiter & midterm II \\
            03/20 &&&&spring break \\
            03/27 &time-segment processing (OLA)&ola&& \\
            04/03 &phase-vocoder&& phase voc&\\
            04/10 &source coding: LPC, ADPCM&&& \\
            04/17 &source coding: Huffman, AAC&&& \\
            04/24 &competition results&&& \\
            %04/29 &---&&& \\
            
		\end{tabular}

        
        %\bigskip
        %Planned exercises include:
        %
        %\begin{tabular}{l}
               %read/write audio files\\
               %ringbuffer\\
               %implement unit tests\\
               %MA filter\\
               %real-time overlapped buffering with variable input size\\
               %convolution\\
               %feature extraction class interface design\\
        %\end{tabular}
    \smallskip
    
        \input{../include/schedule}
    
    \subsection{Academic Integrity}
        \input{../include/integrity}

    \subsection{Accommodations for Individuals with Disabilities}
        \input{../include/disabilities}
    
    %\subsection{Assignment Turn-In}
        %All assignments as well as the project work including code have to be turned in as a link to an online repository such as github. Documentation has to be submitted to t-square unless announced otherwise.
    
    \subsection{Attendance and Participation}
        \input{../include/attendance}
        
    \subsection{Digital Etiquette}
        \input{../include/etiquette}
        
    \subsection{Extensions, Late Assignments, Missed Exams}
        \input{../include/extensions}

    \subsection{Student Use of Mobile Devices in the Classroom}
        \input{../include/mobile}
        
    \subsection{Student-Faculty Expectations}
        \input{../include/expectations}
        
    \subsection{Diversity}
        \input{../include/diversity}
        
    \subsection{Grievances and Concerns}
        \input{../include/grievance}


\end{document}

